print("This task will get the input data from S3 AWS")
import time as tps
import inspect , json

def _error(obj):
    _log(inspect.stack()[1][3],"ERROR",obj)
def _log(who="System",logLevel="Message",obj=""):
    print(tps.strftime("%Y-%m-%d %H:%M:%S"),who,logLevel, obj,sep=" ~ ")

def get_input_from_s3():
    try:
        import boto3
        s3 = boto3.client('s3', aws_access_key_id="",
                          aws_secret_access_key="")
        s3.download_file('elementor-pub', 'Data-Enginner/Challenge1/request1.csv', "request.csv")
    except Exception as ee:
        _error(ee)
        return False
    _log(inspect.stack()[0][3],obj="done")
    return True

def fill_DB_input():
    try:
        import sqlite3
        conn = sqlite3.connect('mainDB.db')
        cursor = conn.cursor()
        cursor.execute("""create table if not exists site 
                            (name TEXT PRIMARY KEY, 
                            updated INTEGER DEFAULT 0 , 
                            site_risk text DEFAULT '')""")
        cursor.execute("""create table if not exists classification 
                            (name TEXT, 
                             value TEXT ,
                             weight integer,
                             PRIMARY KEY(name,value))""")
        conn.commit()
        for lg in open("request.csv"):
            cursor.execute("INSERT OR IGNORE into  site (name) values (%r)"%lg.strip())
            conn.commit()
    except Exception as ee:
        _error(ee)
        return False
    _log(inspect.stack()[0][3], obj="done")
    return True

def update_DB_from_api():
    try:
        import sqlite3
        conn = sqlite3.connect('mainDB.db')
        cursor = conn.cursor()
        for row in cursor.execute("""select * from site order by updated"""):


            if tps.time()-row[1] < 60*30:
                continue

            d =  _callapi(row[0])
            if d == {}:
                continue
            classification = {}
            for provider in d["scans"]:
                provider=d["scans"][provider]

                rst = provider["result"].split(" ")[0]
                classification.setdefault(rst,0)
                classification[rst]+=1

            site_risk = "safe"
            if (classification.get("phishing",0)+
                classification.get("malicious",0) +
                classification.get("malware",0)) :
                site_risk="risk"
            up = conn.cursor()
            qsql = "update site set updated = %d , site_risk= %r  where name=%r" % (tps.time(),site_risk, row[0])
            up.execute(qsql)
            conn.commit()
            up = conn.cursor()
            up.execute("delete from classification where name=%r "%row[0])
            conn.commit()

            for classi in classification:
                up = conn.cursor()
                qsql = "insert or replace into classification values (%r,%r,%d)"%(row[0],classi,classification[classi])
                up.execute(qsql)
                conn.commit()

    except Exception as ee:
        _error((ee))
        return False
    _log(inspect.stack()[0][3], obj="done")
    return True


def _callapi(site):
    import requests as r, json
    vtkey = "2460c2439b7dac13a74afa8188ed198612e881788d93d9cc6eff63475d148b5f"
    url = 'https://www.virustotal.com/vtapi/v2/url/report'
    params = {'apikey': vtkey, 'resource': site}
    response = r.post(url, data=params)
    rst = response.content.decode()
    try :
        return json.loads(rst)
    except:
        return {}
